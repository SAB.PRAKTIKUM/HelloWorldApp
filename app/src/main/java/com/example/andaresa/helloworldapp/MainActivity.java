package com.example.andaresa.helloworldapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    //Deklarasi
    EditText nrp, nama;
    Button submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Inisialisasi object berdasarkan id pada EditText di xml
        nrp=(EditText) findViewById(R.id.nrp);
        nama=(EditText) findViewById(R.id.nama);
        //Instansiasi object berdasarkan id pada Button di xml
        submit=(Button) findViewById(R.id.btn_submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (nrp.getText().toString().length() == 0) {
                    nrp.setError("NRP tidak boleh kosong ");
                } else if (nama.getText().toString().length() == 0) {
                    nama.setError("Nama tidak boleh kosong");
                } else {
                    Toast.makeText(MainActivity.this, "Hello SAB, pertemuan saya " + nama.getText() + " dengan nrp " + nrp.getText(), Toast.LENGTH_SHORT).show();
                }
            }

        });

    }
}

